/**
 * Autor: Shashvina Sivalingam & Piera Blum
 * Date: 19.04.2024
 * Description: This is a main page that directs you to the other pages
 */

import React from "react";
import "./test.css";
import { BrowserRouter as Router, Routes, Route, Link } from "react-router-dom";
import { Vorspeise } from "./vorspeisen";
import { Hauptgerichte } from "./hauptgerichte";
import { Dessert } from "./dessert";

const App = () => (
  <Router>
    <Routes>
      <Route path="/" element={<Home />} />
      <Route path="/vorspeisen" element={<Vorspeise />} />
      <Route path="/hauptgerichte" element={<Hauptgerichte />} />
      <Route path="/dessert" element={<Dessert />} />
    </Routes>
  </Router>
);
const Home = () => (
  <>
    <div className="App">
      <header className="header">
        <h1>Wählen Sie Ihr Gang</h1>
      </header>
      <nav>
        <ul className="size1">
          <Link to="/vorspeisen">Vorspeisen</Link>
        </ul>

        <ul className="size2">
          <Link to="/hauptgerichte">Hauptgerichte</Link>
        </ul>
        <ul className="size3">
          <Link to="/dessert">Dessert</Link>
        </ul>
      </nav>
      <footer id="footer">
        <p>&copy; QR-Code-Projekt</p>
      </footer>
    </div>
  </>
);

export default App;

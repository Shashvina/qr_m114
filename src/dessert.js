/**
 * Autor: Shashvina Sivalingam & Piera Blum
 * Date: 19.04.2024
 * Description: This is an dessert page with a dish image and QR code. You can go to the recipe page using the QR code.
 */

import AmericanCookie from "./Dessert/American_cookie.png";
import churros from "./Dessert/churros.png";
import erdbeerkuchen from "./Dessert/Erdbeerkuchen.png";
import erdbeerroulade from "./Dessert/Erdbeerroulade.png";
import himbeerglace from "./Dessert/Himbeerglace.png";
import kaiserschmarn from "./Dessert/Kaiserschmarn.png";
import macarons from "./Dessert/Macarons.png";
import mousse_au_chocolat from "./Dessert/Mousse_au_chocolat.png";
import waffel from "./Dessert/Waffeln.png";

import chocolat from "./images/dessert/chocolat.png";
import churros1 from "./images/dessert/churros.png";
import cookie from "./images/dessert/cookie.png";
import erdbeerglace from "./images/dessert/erdbeerglace.png";
import erdbeerkuchen1 from "./images/dessert/erdbeerkuchen.png";
import erdbeerroulade1 from "./images/dessert/erdbeerroulade.png";
import kaiserrschmarn from "./images/dessert/kaiserschmarrn.png";
import macarons1 from "./images/dessert/macarons.png";
import waffel1 from "./images/dessert/waffel.png";

import "./App.css";
import React from "react";

export const Dessert = () => {
  return (
    <>
      <header className="header">
        <h1>Wählen Sie Ihr Dessert</h1>
      </header>
      <div className="container">
        <div className="images">
          <ul>
            <li>
              <img src={cookie} alt="cookie" />
              <span>American Cookie</span>
            </li>
            <li>
              <img src={churros1} alt="churos" />
              <span>Churros</span>
            </li>
            <li>
              <img src={erdbeerkuchen1} alt="erdbeerkuchen" />
              <span>Erdbeerkuchen</span>
            </li>
            <li>
              <img src={erdbeerroulade1} alt="erbeerroulade" />
              <span>Erdbeerroulade</span>
            </li>
            <li>
              <img src={erdbeerglace} alt="himbeerglace" />
              <span>Himbeerglace</span>
            </li>
            <li>
              <img src={kaiserrschmarn} alt="kaiserschmarn" />
              <span>Kaiserrschmarn</span>
            </li>
            <li>
              <img src={macarons1} alt="macarons" />
              <span>Macarons</span>
            </li>
            <li>
              <img src={chocolat} alt="chocolat" />
              <span>Mousse au chocolat</span>
            </li>
            <li>
              <img src={waffel1} alt="waffel" />
              <span>Waffeln</span>
            </li>
          </ul>
        </div>
        <ul className="menu">
          <li>
            <div className="item"></div>
            <img src={AmericanCookie} alt="AmericanCookie" />
            <span>American Cookie</span>
          </li>
          <li>
            <img src={churros} alt="churros" />
            <span>Churros</span>
          </li>
          <li>
            <img src={erdbeerkuchen} alt="erdbeerkuchen" />
            <span>Erdbeerkuchen</span>
          </li>
          <li>
            <img src={erdbeerroulade} alt="erdbeerroulade" />
            <span>Erdbeerroulade</span>
          </li>
          <li>
            <img src={himbeerglace} alt="himbeerglace" />
            <span>Himbeerglace</span>
          </li>
          <li>
            <img src={kaiserschmarn} alt="kaiserschmarn" />
            <span>Kaiserrschmarn</span>
          </li>
          <li>
            <img src={macarons} alt="macarons" />
            <span>Macarons</span>
          </li>
          <li>
            <img src={mousse_au_chocolat} alt="mousse_au_chocolat" />
            <span>Mousse au chocolat</span>
          </li>
          <li>
            <img src={waffel} alt="waffel" />
            <span>Waffeln</span>
          </li>
        </ul>
      </div>
      <footer id="footer">
        <p>&copy; QR-Code-Projekt</p>
      </footer>
    </>
  );
};

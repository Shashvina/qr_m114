/**
 * Autor: Shashvina Sivalingam & Piera Blum
 * Date: 19.04.2024
 * Description: This is a main meal page with a dish image and QR code. You can go to the recipe page using the QR code.
 */

import ÄlplermagronenMitApfelmus from "./Hauptgerrichte/ÄlplermagronenMitApfelmus.png";
import Hamburger from "./Hauptgerrichte/Hamburger.png";
import Kartoffelgratin from "./Hauptgerrichte/Kartoffelgratin.png";
import Lasagne from "./Hauptgerrichte/Lasagne.png";
import MahMeh from "./Hauptgerrichte/MahMeh.png";
import PestoPizzaMitCherrytomaten from "./Hauptgerrichte/PestoPizzaMitCherrytomaten.png";
import PouletStroganoff from "./Hauptgerrichte/PouletStroganoff.png";
import RizCasimir from "./Hauptgerrichte/RizCasimir.png";
import SpaghettiCarbonara from "./Hauptgerrichte/SpaghettiCarbonara.png";

import alpe from "./images/haupt/alpe.png";
import burger from "./images/haupt/burger.png";
import kartoffell from "./images/haupt/kartoffell.png";
import lasagne from "./images/haupt/lasagne.png";
import mameh from "./images/haupt/mameh.png";
import pizza from "./images/haupt/pizza.png";
import rizcasimir from "./images/haupt/rizcasimir.png";
import spaghetti from "./images/haupt/spaghetti.png";
import stroganoff from "./images/haupt/stroganoff.png";

import "./App.css";
import React from "react";
export const Hauptgerichte = () => {
  return (
    <>
      <header className="header">
        <h1>Wählen Sie Ihr Hauptgang</h1>
      </header>
      <div className="container">
        <div className="images">
          <ul>
            <li>
              <img src={alpe} alt="alpe" />
              <span>Älplermagronen mit Apfelmus</span>
            </li>
            <li>
              <img src={burger} alt="burger" />
              <span>Hamburger</span>
            </li>
            <li>
              <img src={kartoffell} alt="kartoffell" />
              <span>Kartoffelgratin</span>
            </li>
            <li>
              <img src={lasagne} alt="lasagne" />
              <span>Lasagne</span>
            </li>
            <li>
              <img src={mameh} alt="mameh" />
              <span>MahMeh</span>
            </li>
            <li>
              <img src={pizza} alt="pizza" />
              <span>Pesto-Pizza mit Cherrytomaten</span>
            </li>
            <li>
              <img src={stroganoff} alt="stroganoff" />
              <span>Stroganoff</span>
            </li>
            <li>
              <img src={rizcasimir} alt="rizcasimir" />
              <span>RizCasimir</span>
            </li>
            <li>
              <img src={spaghetti} alt="spaghetti" />
              <span>Spaghetti Carbonara</span>
            </li>
          </ul>
        </div>

        <ul className="menu">
          <li>
            <div className="item"></div>
            <img
              src={ÄlplermagronenMitApfelmus}
              alt="ÄlplermagronenMitApfelmus"
            />
            <span>Älplermagronen mit Apfelmus</span>
          </li>

          <li>
            <img src={Hamburger} alt="Hamburger" />
            <span>Hamburger</span>
          </li>

          <li>
            <img src={Kartoffelgratin} alt="Kartoffelgratin" />
            <span>Kartoffelgratin</span>
          </li>

          <li>
            <img src={Lasagne} alt="Lasagne" />
            <span>Lasagne</span>
          </li>

          <li>
            <img src={MahMeh} alt="MahMeh" />
            <span>Mah-Meh</span>
          </li>

          <li>
            <img
              src={PestoPizzaMitCherrytomaten}
              alt="PestoPizzaMitCherrytomaten"
            />
            <span>Pesto-Pizza mit Cherrytomaten</span>
          </li>

          <li>
            <img src={PouletStroganoff} alt="PouletStroganoff" />
            <span>Poulet-Stroganoff</span>
          </li>

          <li>
            <img src={RizCasimir} alt="RizCasimir" />
            <span>Riz Casimir</span>
          </li>

          <li>
            <img src={SpaghettiCarbonara} alt="SpaghettiCarbonara" />
            <span>Spaghetti Carbonara</span>
          </li>
        </ul>
      </div>
      <footer id="footer">
        <p>&copy; QR-Code-Projekt</p>
      </footer>
    </>
  );
};

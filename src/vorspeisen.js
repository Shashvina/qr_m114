/**
 * Autor: Shashvina Sivalingam & Piera Blum
 * Date: 19.04.2024
 * Description: This is an appetiser page with a dish image and QR code. You can go to the recipe page using the QR code.
 */

import Bruschetta from "./Vorspeisen/Bruschetta.png";
import Kürbissuppe from "./Vorspeisen/Kürbissuppe.png";
import Pizza from "./Vorspeisen/Pizza.png";
import ToastHawaii from "./Vorspeisen/ToastHawaii.png";
import WienerliImTeig from "./Vorspeisen/WienerliImTeig.png";
import ButtermilchSuppe from "./Vorspeisen/Buttermilch-suppe.png";
import CroqueMonsieur from "./Vorspeisen/Croque_Monsieur.png";
import KäseCervelatSalat from "./Vorspeisen/Käse_Cervelat_salat.png";
import Schinkengipfeli from "./Vorspeisen/Schinkengipfeli.png";

import bruschetta1 from "./images/vorspeise/bruschetta.png";
import buttermilch from "./images/vorspeise/buttermilch.png";
import cervelat from "./images/vorspeise/cervelat.png";
import kürbis from "./images/vorspeise/kürbis.png";
import monsieur from "./images/vorspeise/monsieur.png";
import schinken from "./images/vorspeise/schinken.png";
import schneken from "./images/vorspeise/schneken.png";
import toast from "./images/vorspeise/toast.png";
import wienerli from "./images/vorspeise/wienerli.png";

import "./App.css";
import React from "react";

export const Vorspeise = () => {
  return (
    <>
      <header className="header">
        <h1>Wählen Sie Ihre Lieblingsvorspeise</h1>
      </header>
      <div className="container">
        <div className="images">
          <ul>
            <li>
              <img src={bruschetta1} alt="Bruschetta" />
              <span>Bruschetta</span>
            </li>
            <li>
              <img src={kürbis} alt="kürbis" />
              <span>Kürbissuppe</span>
            </li>
            <li>
              <img src={schneken} alt="schneken" />
              <span>Pizza-Schnecken</span>
            </li>
            <li>
              <img src={toast} alt="toast" />
              <span>Toast-Hawaii</span>
            </li>
            <li>
              <img src={wienerli} alt="wienerli" />
              <span>Wienerli im Teig</span>
            </li>
            <li>
              <img src={buttermilch} alt="buttermilch" />
              <span>Buttermilch-Suppe</span>
            </li>
            <li>
              <img src={monsieur} alt="monsieur" />
              <span>Croque Monsieur</span>
            </li>
            <li>
              <img src={schinken} alt="schinken" />
              <span>Käse Cervelat Salat</span>
            </li>
            <li>
              <img src={cervelat} alt="cervelat" />
              <span>Schinkengipfeli</span>
            </li>
          </ul>
        </div>

        <ul className="menu">
          <li>
            <div className="item"></div>
            <img src={Bruschetta} alt="Bruschetta" />
            <span>Bruschetta</span>
          </li>

          <li>
            <img src={Kürbissuppe} alt="Kürbissuppe" />
            <span>Kürbissuppe</span>
          </li>

          <li>
            <img src={Pizza} alt="Pizza" />
            <span>Pizza-Schnecken</span>
          </li>

          <li>
            <img src={ToastHawaii} alt="ToastHawaii" />
            <span>Toast-Hawaii</span>
          </li>

          <li>
            <img src={WienerliImTeig} alt="WienerliImTeig" />
            <span>Wienerli im Teig</span>
          </li>

          <li>
            <img src={ButtermilchSuppe} alt="ButtermilchSuppe" />
            <span>Buttermilch-Suppe</span>
          </li>

          <li>
            <img src={CroqueMonsieur} alt="CroqueMonsieur" />
            <span>Croque Monsieur</span>
          </li>

          <li>
            <img src={KäseCervelatSalat} alt="KäseCervelatSalat" />
            <span>Käse Cervelat Salat</span>
          </li>

          <li>
            <img src={Schinkengipfeli} alt="Schinkengipfeli" />
            <span>Schinkengipfeli</span>
          </li>
        </ul>
      </div>
      <footer id="footer">
        <p>&copy; QR-Code-Projekt</p>
      </footer>
    </>
  );
};
